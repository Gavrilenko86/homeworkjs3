const number = prompt("Enter a number:");
if (isNaN(number)) {
  console.log("Invalid input. Please enter a valid number.");
} else {
  const end = parseInt(number);
  let result = "";
  for (let i = 0; i <= end; i++) {
    if (i % 5 === 0) {
      result += i + " ";
    }
  }
  if (result) {
    console.log("Numbers divisible by 5: " + result);
  } else {
    console.log("Sorry, no numbers.");
  }
}
